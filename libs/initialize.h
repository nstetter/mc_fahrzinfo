void initialise_setup(void);
void delay_us(int us);
extern void S0Handler(void);
extern void S1Handler(void);
void write_command(void);
void write_data(void);
void window_set();
void initialise_ssd1963(void);
void clear_screen(int color);


//
void initialise_setup(void)
{
   //Port  Clock Gating
   SYSCTL_RCGCGPIO_R  |= 0x0C00; //clock enable Port M + L
   while((SYSCTL_PRGPIO_R & 0x0C08) == 0); //wait for clock enable
   //Enable Ports
   GPIO_PORTL_DEN_R = 0x1F; //enable Port L 0-4
   GPIO_PORTM_DEN_R = 0xFF; //enable Port M 0-7
   //Set direction
   GPIO_PORTL_DIR_R = 0x1F; //Port L 0-4 = Output
   GPIO_PORTM_DIR_R = 0xFF; //Port M 0-7 = Output

}

/* Description: Delays transmitted time 
 * @param us: delay in micro secondes
 */
void delay_us(int us){
    SysCtlDelay((FCPU/(3*1000000))*us); // 3 "ticks" per seconde
}

/* Description: Tivaware-specific initialization for TIM A,
 * used for interrupt S0 to count the speed and direction of the motor.
 */
void init_timer0(){
    // Enable the TIM0 peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    // Wait for the TIM0 module to be ready.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0))
	// Software Reset of TIM0
    SysCtlPeripheralReset(SYSCTL_PERIPH_TIMER0); 
	// Disable TIM A
    TimerDisable(TIMER0_BASE, TIMER_A);
    //set clock as timer source
    TimerClockSourceSet(TIMER0_BASE, TIMER_CLOCK_SYSTEM);
    //set timer to full width counting up, periodic
    TimerConfigure(TIMER0_BASE,TIMER_CFG_PERIODIC_UP);
    TimerUpdateMode(TIMER0_BASE, TIMER_A, TIMER_UP_LOAD_IMMEDIATE);
	
    delay_us(100); // some delay can't be false :)
	//start timer
    TimerEnable(TIMER0_BASE, TIMER_A);
    TimerLoadSet(TIMER0_BASE, TIMER_A, 0);
}


/* Description: Tivaware-specific initialization for only used interrupt.
 * Used for interrupt S0 to count the speed and direction of the motor.
 */
void init_interrupt(){

    //Port  Clock Gating
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    delay_us(100);

    //Disable interrupts globally
    IntMasterDisable();

    //Enable Pins
    GPIO_PORTP_DEN_R = 0x03; //enable Port P 0-1

    //Set direction
    GPIO_PORTP_DIR_R = 0x00; //Port P 0-7 = Input

    //setup interrupt Pins
    GPIOPadConfigSet(GPIO_PORTP_BASE,GPIO_PIN_0, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD); //set weak pull down
    GPIOPadConfigSet(GPIO_PORTP_BASE,GPIO_PIN_1, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD); //set weak pull down
    GPIOIntTypeSet(GPIO_PORTP_BASE, GPIO_PIN_0, GPIO_RISING_EDGE|GPIO_DISCRETE_INT); //set interrupt to rising edge and enable seperate interrupts for each pin (only port P and Q)
	
    //Set interrupt handlers
    IntRegister(INT_GPIOP0, S0Handler); //register handler to pin P0
    GPIOIntClear(GPIO_PORTP_BASE,GPIO_PIN_0); // optional, but secure is safe ;)
    IntPrioritySet(INT_GPIOP0,0x20); // set priority of INT0

    //Enable Interrupts
    GPIOIntEnable(GPIO_PORTP_BASE, GPIO_INT_PIN_0);  //enable Interrupt on Port P Pin 0
    IntEnable(INT_GPIOP0); //enable interrupt in NVIC
    IntMasterEnable();//Enable interrupts globally
}



/* Description: Product-specific steps required to send a command to the ssd1963
 */
void write_command(void)
{
    GPIO_PORTL_DATA_R = 0x1F; //initial state
    GPIO_PORTL_DATA_R &= ~(PIN_CS); //CS=0
    GPIO_PORTL_DATA_R &= ~(PIN_RS); //RS=0
    GPIO_PORTL_DATA_R &= ~(PIN_WR); //WR=0
    GPIO_PORTM_DATA_R = g_window.command; //write command
    GPIO_PORTL_DATA_R |= PIN_WR; //WR=1
    GPIO_PORTL_DATA_R |= PIN_CS; //CS=1
}


/* Description: Product-specific steps required to write data to the ssd1963
 */
void write_data(void)
{
    GPIO_PORTL_DATA_R = 0x1F; //initial state
    GPIO_PORTL_DATA_R &= ~(PIN_CS); //CS=0
    GPIO_PORTL_DATA_R |= (PIN_RS); //RS=1
    GPIO_PORTL_DATA_R &= ~(PIN_WR); //WR=0
    GPIO_PORTM_DATA_R = g_window.data; //write data
    GPIO_PORTL_DATA_R |= PIN_WR; //WR=1
    GPIO_PORTL_DATA_R |= PIN_CS; //CS=1
}

/* Description: Put the pointer on the place where you want to set the rgb value
 */
void window_set()
{
    	/*x coordinate*/
        g_window.command = 0x2A;
        write_command(); //set page address
        g_window.data = g_window.start_x >> 8;
        write_data(); //set start page address HB
        g_window.data = g_window.start_x;
        write_data(); //set start page address LB
        g_window.data = g_window.end_x >> 8;
        write_data(); //set end page address HB
        g_window.data = g_window.end_x;
        write_data(); //set end page address LB

        /*y coordinate*/
        g_window.command = 0x2B;
        write_command(); //set column address
        g_window.data = g_window.start_y>>8;
        write_data(); //set start column address HB
        g_window.data = g_window.start_y;
        write_data(); //set start column address LB
        g_window.data = g_window.end_y>>8;
        write_data(); //set end column address HB
        g_window.data = g_window.end_y;
        write_data(); //set end column address LB
}

/* Description: Product-specific initialization of the ssd1963.
 * Necessary at start of using the touch screen
 */
void initialise_ssd1963(void)
{
    g_window.height = 272; //set window height
    g_window.width = 480;  //set window width
	////
    GPIO_PORTL_DATA_R = 0x00; //RST=0
    delay_us(100); //wait > 100u
    ////
    g_window.command = 0x01;
    write_command(); //software reset
    delay_us(5000); //wait >5m
    ////
    g_window.command = 0xE2;
    write_command(); //sett PLL f=100Mhz
    g_window.data = 0x1D;
    write_data();
    g_window.data = 0x02;
    write_data();
    g_window.data = 0x04;
    write_data();
    ////
    g_window.command = 0xE0;
    write_command(); //start PLL
    g_window.data =0x01 ;
    write_data();
    delay_us(100); //wait > 100u
    ////
    g_window.command = 0xE0;
    write_command(); //lock PLL
    g_window.data = 0x03 ;
    write_data();
    delay_us(100); //wait > 100us
    ////
    g_window.command = 0x01;
    write_command(); //software reset
    delay_us(5000); //wait >5m
    ////
    g_window.command = 0xE6;
    write_command(); //set LCD pixel CLK = 9 MHz
    g_window.data = 0x01;
    write_data();
    g_window.data = 0x70;
    write_data();
    g_window.data =0xA3 ;
    write_data();
    g_window.command = 0xB0;
    write_command(); //set LCD panel mode
    g_window.data = 0x20;
    write_data(); //TFT panel 24bit
    g_window.data = 0x00;
    write_data(); //TFT mode
    g_window.data = 0x01;
    write_data(); //horizontal size 480-1 HB
    g_window.data = 0xDF;
    write_data(); //horizontal size 480-1 LB
    g_window.data = 0x01 ;
    write_data(); //vertical size 272-1 HB
    g_window.data = 0x0F ;
    write_data(); //vertical size 272-1 LB
    g_window.data = 0x00 ;
    write_data(); //even/odd line RGB
    ////
    g_window.command = 0xB4;
    write_command(); //set horizontal period
    g_window.data =0x02 ;
    write_data(); //set HT total pxel = 531 HB
    g_window.data = 0x13;
    write_data(); //LB
    g_window.data = 0x00;
    write_data(); //set horiz sync start = 43 HB
    g_window.data =0x2B ;
    write_data(); //LB
    g_window.data = 0x0A;
    write_data(); //set horiz sync = 10
    g_window.data = 0x00;
    write_data(); //set horiz sync start pos = 8 HB
    g_window.data = 0x08;
    write_data(); //LB
    g_window.data = 0x00;
    write_data();
    ////
    g_window.command = 0xB6;
    write_command(); //set vertical period
    g_window.data = 0x01;
    write_data(); //set VT lines = 288 HB
    g_window.data = 0x20;
    write_data(); //LB
    g_window.data = 0x00;
    write_data(); //set VPS = 12 HB
    g_window.data = 0x0C;
    write_data(); //LB
    g_window.data = 0x0A;
    write_data(); //vertical sync pulse = 10
    g_window.data = 0x00 ;
    write_data(); //vert sync pulse start = 4 HB
    g_window.data = 0x04;
    write_data(); //LB
    ////
    g_window.command = 0x36;
    write_command(); //flip display
    g_window.data = 0x03;
    write_data();
    ////
    g_window.command = 0xF0;
    write_command(); //pixel data format = 8bit
    g_window.data = 0x00 ;
    write_data();
    ////
    g_window.command = 0x29;
    write_command(); //display on

}

/* Description: By the initialization the display is filled with "snow".
 * In this function, a uniform background color is set. Default is white.
 */
void clear_screen(int color)
{
	// set width of the screen
    g_window.start_x = 0;
    g_window.end_x = 479;
	// set height of the screen
    g_window.start_y = 0;
    g_window.end_y = 271;
	// RGB shares to max cause of white uniform background
    g_window.red = (color>>(8*2)&0xFF); //red value 
    g_window.green = (color>>(8*1)&0xFF); //green value
    g_window.blue = (color>>(8*0)&0xFF); //blue value
    window_set();
    g_window.command = 0x2C;
    write_command();
    // set every Pixel to background color
	int i;
    for(i = 0; i < g_window.height*g_window.width; i++){
            g_window.data = g_window.red; // reference data to red value
            write_data(); // send data to display
            g_window.data = g_window.green; // reference data to green value
            write_data();
            g_window.data = g_window.blue; // reference data to blue value
            write_data();

        }

}


