/*
 * window.h
 *
 *  Created on: 01.11.2017
 *      Author: enste
 */

#define COL_BLACK 0x000000
#define COL_WHITE 0xFFFFFF
#define COL_BACK 0x000000

void set_pixel(unsigned int x, unsigned int y, unsigned long color);
void drawChar(int x0, int y0, int color, int chars[]);
void drawLine(int x0, int y0, int x1, int y1, int color);
void drawLabels(int color);
void drawNeedle();
void drawHalfCircle(int r, int color);
void drawCounter(int km, int x, int y, int color);

char directionFlag = 0; // Auxiliary variable for a speed display

/**/
typedef struct SETPIXEL{
    // dimensions of the display
    int height;
    int width;
    //command & data for writing to display
    int command; // 2 Byte command
    int data; // 2 Byte command
    // set courser
    int start_x;
    int start_y;
    int end_x;
    int end_y;
    int red;   // 1 Byte color
    int green;
    int blue;
} window;
volatile window g_window;





/*
 * Description:  Set a single Pixel for the SSD1963
 * @param x:     x-coordinate of pixel
 * @param y:     y-coordinate of pixel
 * @param color: color of pixel
 *
 */
void set_pixel(unsigned int x, unsigned int y, unsigned long color)
{
    /*set start coordinates*/
    g_window.start_x = x;
    g_window.end_x = x;
    g_window.start_y = y;
    g_window.end_y = y;
    window_set();
    /*write command to set pixel*/
    g_window.command = 0x2C;
    write_command();
    /*transmit color value*/
    g_window.data = (color>>(8*2)&0xFF); //red value
    write_data();
    g_window.data = (color>>(8*1)&0xFF); //green value
    write_data();
    g_window.data = (color>>(8*0)&0xFF); //blue value
    write_data();
}

/* Description: Draws a transferred character to the specified position on the display.
 * @param x0: start x coordinate of the character
 * @param y0: start y coordinate of the character
 * @param color: color in which the character is displayed
 * @param chars: character that should displayed
 */
void drawChar(int x0, int y0, int color, int chars[]){
int x;
int y;
for (y = 0; y < font_y; y++){ //height of every character
    for(x = 0; x < font_x; x++){ //width of every character
        if(((chars[y]) << x) & 0x800){ //bit-shifting to the left and masking to get the respective bit
            set_pixel(x0 + x, y0 + y, color); //if bit equals 1, set it in given color to current point
            }
        }
    }
}


/* Bresenham Implementation from de.wikipedia
 * Description: Draws a linie to the display
 * @param x0: start x coordinate of the linie
 * @param y0: start y coordinate of the linie
 * @param x: end x coordinate of the linie
 * @param y: end y coordinate of the linie
 * @param color: color in which the linie is displayed
 */
void drawLine(int x0, int y0, int x1, int y1, int color)
{
	  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
	  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
	  int err = dx+dy, e2; /* error value e_xy */

	  while(1){
		set_pixel(x0,y0,color);
		if (x0==x1 && y0==y1) break; // end drawLine
		e2 = 2*err;
		if (e2 > dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
		if (e2 < dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
	  }

}


/* Description: Draws labels to the speedometer
 * @param color: color in which the labels are displayed
 */
void drawLabels(int color)
{
	int x_offset = 12; // width of character
	int y_offset = -16; // width of character
	int degree = 0; // get the point from the angle of the circle 

	//0km/h
	drawChar((circ_x[degree] - font_x*1 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//50km/h
	degree = 22;
	drawChar((circ_x[degree] - font_x*2 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_5);
	drawChar((circ_x[degree] - font_x*1 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//100km/h
	degree = 45;
	drawChar((circ_x[degree] - font_x*3 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_1);
	drawChar((circ_x[degree] - font_x*2 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	drawChar((circ_x[degree] - font_x*1 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//150km/h
	degree = 71;
	drawChar((circ_x[degree] - font_x*3 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_1);
	drawChar((circ_x[degree] - font_x*2 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_5);
	drawChar((circ_x[degree] - font_x*1 - x_offset), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//200km/h
	degree = 89;
	drawChar((circ_x[degree] - font_x*1), (circ_y[degree] + y_offset - 5), color, g_chars.CHARS_2);
	drawChar((circ_x[degree])           , (circ_y[degree] + y_offset - 5), color, g_chars.CHARS_0);
	drawChar((circ_x[degree] + font_x*1), (circ_y[degree] + y_offset - 5), color, g_chars.CHARS_0);
	//250km/h
	degree = 112;
	rawChar((circ_x[degree] + font_x*1), (circ_y[degree] + y_offset), color, g_chars.CHARS_2);
	drawChar((circ_x[degree] + font_x*2), (circ_y[degree] + y_offset), color, g_chars.CHARS_5);
	drawChar((circ_x[degree] + font_x*3), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//300km/h
	degree = 135;
	drawChar((circ_x[degree] + font_x*1), (circ_y[degree] + y_offset), color, g_chars.CHARS_3);
	drawChar((circ_x[degree] + font_x*2), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	drawChar((circ_x[degree] + font_x*3), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//350km/h
	degree = 158;
	drawChar((circ_x[degree] + font_x*1), (circ_y[degree] + y_offset), color, g_chars.CHARS_3);
	drawChar((circ_x[degree] + font_x*2), (circ_y[degree] + y_offset), color, g_chars.CHARS_5);
	drawChar((circ_x[degree] + font_x*3), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	//400km/h 
	degree = 179;
	drawChar((circ_x[degree] + font_x*1), (circ_y[degree] + y_offset), color, g_chars.CHARS_4);
	drawChar((circ_x[degree] + font_x*2), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
	drawChar((circ_x[degree] + font_x*3), (circ_y[degree] + y_offset), color, g_chars.CHARS_0);
}

/* Description: Displays the speedometer needle
 * @param rad: rotate the needle by the given rad around the center of the speedometer
 */
void drawNeedle(int rad){

    static int needleOld;
	// check ranges and correct them if needed
    if(rad <= 0) rad = 0; // rad to low 
    if(rad >= 179) rad = 179; // rad to high

    if(needleOld != rad){ // if needle position has changed
		drawLine(cx,cy,circ_x[needleOld],circ_y[needleOld],COL_BACK);  //overwrite old line
		drawLine(cx,cy,circ_x[rad],circ_y[rad],COL_WHITE);            //draw new line
	}
	needleOld = rad; // safe old rotation value for overwriting at next function call
}

/* Description: Displays the basic speedometer framework
 * @param r: rad of the outermost line
 * @param color: color of the basic framework
 */
void drawHalfCircle(int r, int color){
    int i;
    int k;

    for(k = 5; k > 0; k--){ //4 iterations for line thickness
        calculate_circle((r-k)); //calculate circle coordinates
        for(i = 0; i < degrees; i++){ // draw pixel for each calculated point
            set_pixel(circ_x[i],circ_y[i], color);
        }
    }
    drawLabels(color); // display labels of the speedometer
    calculate_circle((r-15)); //calculate circle for needle
}


/* Description: displays odometer at screen
 * @param km: traveled distance in km
 * @param x: start-x-coordinate of the odometer 
 * @param y: start-y-coordinate of the odometer
 * @param color: color in which the odometer is displayed
 */
void drawCounter(int km, int x, int y, int color){

// alignment stuff
int y_offset = 25;
int x_offset = (0.5*font_x);
int x_0 = (x - x_offset - font_x * 4);
int digits[5]={}; // safe digits here
int i = sizeof(digits)/sizeof(digits[0])-1; // safe for indexstuff



while(km>0)
    { // split km into digits
        digits[i] = km % 10 ; // beginning with first digit
        km /= 10; // divided by 10 to get next 10. potenz
        i--;
    }

    //digit 0
    drawChar((x_0 + font_x * 0), (y + y_offset), COL_BACK, g_chars.CHARS_CLEAR);
    drawChar((x_0 + font_x * 0), (y + y_offset), color, mapDigit(digits[0]));
    //digit 1
    drawChar((x_0 + font_x * 1), (y + y_offset), COL_BACK, g_chars.CHARS_CLEAR);
    drawChar((x_0 + font_x * 1), (y + y_offset), color, mapDigit(digits[1]));
    //digit 2
    drawChar((x_0 + font_x * 2), (y + y_offset), COL_BACK, g_chars.CHARS_CLEAR);
    drawChar((x_0 + font_x * 2), (y + y_offset), color, mapDigit(digits[2]));
    //comma
    drawChar((x_0 + font_x * 3), (y + y_offset), color, g_chars.CHARS_COMMA);
    //digit 3
    drawChar((x_0 + font_x * 4), (y + y_offset), COL_BACK, g_chars.CHARS_CLEAR);
    drawChar((x_0 + font_x * 4), (y + y_offset), color, mapDigit(digits[3]));
    //digit 4
    drawChar((x_0 + font_x * 5), (y + y_offset), COL_BACK, g_chars.CHARS_CLEAR);
    drawChar((x_0 + font_x * 5), (y + y_offset), color, mapDigit(digits[4]));
    //space
    drawChar((x_0 + font_x * 6), (y + y_offset), color, g_chars.CHARS_SPACE);
    //k
    drawChar((x_0 + font_x * 7), (y + y_offset), color, g_chars.CHARS_K);
    //m
    drawChar((x_0 + font_x * 8), (y + y_offset), color, g_chars.CHARS_M);
}

/* Description: displays current motion direction of the motor
 * @param color: color in which the direction is displayed
 */
void drawDirection(int color){
    if(directionFlag == 0){ //Right
        drawChar((g_window.width - 20- font_x), 10, color, g_chars.CHARS_R); // displays at the upper right corner
        drawChar(20, 10, COL_BLACK, g_chars.CHARS_L); // displays at the upper left corner
    }
    else if(directionFlag == 1){ //Left
        drawChar(20, 10, color, g_chars.CHARS_L); // displays at the upper left corner
        drawChar((g_window.width - 20- font_x), 10, COL_BLACK, g_chars.CHARS_R);
    }
}



/* Description: Assign the numbers to the representative bit vector
 * @param color: digits that should be mapped
 */
int * mapDigit(int digit){
    switch(digit){
    case 0: return g_chars.CHARS_0; break; // draw  0 to display
    case 1: return g_chars.CHARS_1; break; // draw  1 to display
    case 2: return g_chars.CHARS_2; break; // draw  2 to display
    case 3: return g_chars.CHARS_3; break; // draw  3 to display
    case 4: return g_chars.CHARS_4; break; // draw  4 to display
    case 5: return g_chars.CHARS_5; break; // draw  5 to display
    case 6: return g_chars.CHARS_6; break; // draw  6 to display
    case 7: return g_chars.CHARS_7; break; // draw  7 to display
    case 8: return g_chars.CHARS_8; break; // draw  8 to display
    case 9: return g_chars.CHARS_9; break; // draw  9 to display
    }
}


