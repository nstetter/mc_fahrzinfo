/*---------------------------------------------------------------------------------------
 * FahrzeugInfoDisplay:
    Read speed and direction from rotary encoder and print to LCD
 *                                                                    Agrebi, Stetter 2017
 *---------------------------------------------------------------------------------------
*/

//MACROS
#define PIN_RD 0x01
#define PIN_WR 0x02
#define PIN_RS 0x04
#define PIN_CS 0x08
#define PIN_RST 0x10

#define FCPU 16000000u


#include <stdint.h>
#include <stdbool.h>
#include "stdio.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "utils/uartstdio.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"


#include "tm4c1294ncpdt.h"


#include "libs/font.h"
#include "libs/circle.h"
#include "libs/window.h"
#include "libs/initialize.h"



//Global variables
uint32_t timeNew = 0;
uint32_t timeOld = 0;
double speed = 0; //current motor speed
int speedRad = 0; //current motor speed converted to rad
static int distance = 0; //current traveled distance


/* Description: Interrupt handler for handling speed and speed motion.
 * Trigger source is Port P GPIO0
 */
extern void S0Handler(void)
{
    //speed
    GPIOIntClear(GPIO_PORTP_BASE,GPIO_PIN_0);
    timeNew = TimerValueGet(TIMER0_BASE, TIMER_A);
    speed = (timeNew-timeOld); //get time since last interrupt call
	//calculate speed out of timedifference
    speed /= FCPU; 
    speed = 3.6/speed;
    speedRad = speed*0.45;
	
    TimerLoadSet(TIMER0_BASE, TIMER_A, 0); //reset timer 
    timeOld = timeNew; //safe old timer value

    //direction test if S2 is high, when interrupt on S1 is triggered
    if((GPIO_PORTP_DATA_R & 0x02) >> 1 == 0) directionFlag = 0;
    else directionFlag = 1;
	
    distance++; //increase distance
}


int main(void)
{
	//initialization
    initialise_setup(); //initialize peripherals
    initialise_ssd1963(); //initialize display
	delay_us(2000); //some delay
    clear_screen(COL_BLACK); //paint background on screen
    delay_us(5000); //some delay
    drawHalfCircle(170, COL_WHITE); //paint basic speedometer framework
    init_interrupt(); //initialize interrupt
    init_timer0(); //initialize timer


    while(1){
        drawNeedle(speedRad); // display current speed with the analog needle
        drawDirection(COL_WHITE); // display current speed motion with character "R" for right or "L" for left
        drawCounter((distance/10), cx, cy, COL_WHITE); // display current traveled distance in km
    }

}
